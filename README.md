# The Fever Map

There is widespread distrust and confusion about the true number of COVID-19 patients out there. Most patients do not contact hospitals or clinics and never receive a diagnosis. Their number remains unknown to this day. It is important that these patients groups continue to leave health services alone so that they remain available for the patients that really need it. But this leaves us guessing the statistics, wondering at how many uncharted patients there really are.

FeverMap is the answer to this need for clarity. An app that gathers anonymous data about its users’ fever and respiratory symptoms over the duration of their illness.  The goal is to build a real-time map that displays the nations’ infection levels accurately and easily.
With FeverMap’s data, authorities can create appropriate plans of action and make information-based decisions that keep us one step ahead of the problem. Users can feel safer knowing what areas to avoid and know to exercise heightened caution in risk areas.

We are proud to give you FeverMap, the future of tracking epidemic and pandemic diseases.


## How to participate?

Sign up at https://fevermap.net/

## How to contribute?

Browse issues at https://gitlab.com/groups/fevermap/-/issues and chip in the discussion!
